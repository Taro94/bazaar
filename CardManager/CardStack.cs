﻿using System;

namespace CardManager
{
	public class CardStack : ICloneable
	{
		private int _quantity;

		public CardStack(string name, string set, int instance, int quantity, int quantityInUse, bool foil = false)
		{
			Name = name;
			Set = set;
			Instance = instance;
			Quantity = quantity;
			QuantityInUse = quantityInUse;
			Foil = foil;

			ImageFileName = Name.Replace(" // ", "") + ".full.jpg";
		}

		public string Name { get; private set; }
		public string Set { get; private set; }
		public int Instance { get; private set; }
		public int QuantityInUse { get; private set; }
		public bool Foil { get; private set; }
		public string ImageFileName { get; private set; }
		public string ImageFileNameWithInstance
		{
			get
			{
				return ImageFileName.Replace(".full.jpg", $"{Instance}.full.jpg");
			}
		}
		public int Quantity
		{
			get => _quantity;
			set
			{
				if (value < QuantityInUse)
					throw new InvalidOperationException("Attempt to set card quantity to a value below quantity in use.");
				_quantity = value;
			}
		}

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		public CardStack TakeCardFromStack(int numberOfCardsTaken = 1)
		{
			Quantity -= numberOfCardsTaken;
			return new CardStack(Name, Set, Instance, numberOfCardsTaken, 0, Foil);
		}

		public void AddCardToStack(int numberOfCardsAdded = 1)
		{
			Quantity += numberOfCardsAdded;
		}
	}
}
