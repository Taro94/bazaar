﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace CardManager
{
	public class GZip
	{
		public Stream ExtractGZip(Stream source)
		{
			MemoryStream decompressedStream = new MemoryStream();
			using (GZipStream decompressionStream = new GZipStream(source, CompressionMode.Decompress))
			{
				decompressionStream.CopyTo(decompressedStream);
			}
			decompressedStream.Seek(0, SeekOrigin.Begin);
			return decompressedStream;
		}

		public Stream CreateGZip(Stream source)
		{
			MemoryStream compressedStream = new MemoryStream();
			using (GZipStream gzipStream = new GZipStream(compressedStream, CompressionMode.Compress, true))
			{
				source.CopyTo(gzipStream);
			}
			compressedStream.Seek(0, SeekOrigin.Begin);
			return compressedStream;
		}

		public void ExtractGZip(string sourcePath, string targetPath)
		{
			FileInfo gzipFileName = new FileInfo(sourcePath);
			using (FileStream fileToDecompressAsStream = gzipFileName.OpenRead())
			{
				string decompressedFileName = targetPath;
				using (FileStream decompressedStream = File.Create(decompressedFileName))
				{
					using (GZipStream decompressionStream = new GZipStream(fileToDecompressAsStream, CompressionMode.Decompress))
					{
						try
						{
							decompressionStream.CopyTo(decompressedStream);
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.Message);
						}
					}
				}
			}
		}

		public void CreateGZip(string tgzFilename, string targetPath)
		{
			FileInfo fileToBeGZipped = new FileInfo(tgzFilename);
			FileInfo gzipFileName = new FileInfo(targetPath);

			using (FileStream fileToBeZippedAsStream = fileToBeGZipped.OpenRead())
			{
				using (FileStream gzipTargetAsStream = gzipFileName.Create())
				{
					using (GZipStream gzipStream = new GZipStream(gzipTargetAsStream, CompressionMode.Compress))
					{
						try
						{
							fileToBeZippedAsStream.CopyTo(gzipStream);
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.Message);
						}
					}
				}
			}
		}
	}
}
