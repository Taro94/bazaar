﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace CardManager
{
	public class Quest
	{
		private XDocument _xmlBody;

		public Quest(Stream stream)
		{
			_xmlBody = XDocument.Load(stream);
			if (_xmlBody.Descendants("credits").FirstOrDefault() == null || _xmlBody.Descendants("cardPool").FirstOrDefault() == null)
				throw new XmlException("Incorrect XML Forge quest file");
			Cards = new List<CardStack>();
			NewCards = new List<CardStack>();
			_LoadCardsToList(_xmlBody, Cards);
			_LoadNewCardsToList(_xmlBody, NewCards);
			Name = _GetQuestName(_xmlBody);
		}

		public int Gold
		{
			get { return _GetGoldInXDocument(_xmlBody); }
			set { _SetGoldInXDocument(_xmlBody, value); }
		}
		public List<CardStack> Cards { get; set; }
		public List<CardStack> NewCards { get; set; }
		public string Name { get; private set; }

		public MemoryStream ToMemoryStream()
		{
			_CardsToXml(Cards, _xmlBody);
			_NewCardsToXml(NewCards, _xmlBody);
			var stream = new MemoryStream();
			using (var writer = new StreamWriter(stream, new UTF8Encoding(false)))
				_xmlBody.Save(writer);
			return new MemoryStream(stream.ToArray());
		}

		private static int _GetGoldInXDocument(XDocument doc)
		{
			return int.Parse(doc.Descendants("credits").First().Value);
		}

		private static bool _SetGoldInXDocument(XDocument doc, int gold)
		{
			var credits = doc.Descendants("credits").First();
			if (gold < 0)
				return false;
			credits.SetValue(gold);
			return true;
		}

		private string _GetQuestName(XDocument doc)
		{
			string name = doc.Descendants("name").First().Value;
			return name;
		}

		private int _CardCopiesRequiredInDecks(XDocument doc, string name, string set, int instance)
		{
			int copiesRequired = 0;
			foreach (var deck in doc.Descendants("forge.deck.Deck"))
			{
				int copiesRequiredInDeck = 0;
				foreach (var card in deck.Descendants("card").Where(x => x.Attribute("c").Value == name && x.Attribute("s").Value == set && x.Attribute("i").Value == instance.ToString()))
					copiesRequiredInDeck += int.Parse(card.Attribute("n").Value);
				if (copiesRequiredInDeck > copiesRequired)
					copiesRequired = copiesRequiredInDeck;
			}
			return copiesRequired;
		}

		private void _LoadCardsToList(XDocument doc, List<CardStack> cards)
		{
			_LoadCardsFromNodeToList(doc, cards, "cardPool");
		}

		private void _LoadNewCardsToList(XDocument doc, List<CardStack> cards)
		{
			_LoadCardsFromNodeToList(doc, cards, "newCardList");
		}

		private void _LoadCardsFromNodeToList(XDocument doc, List<CardStack> cards, string nodeName)
		{
			var cardPool = doc.Descendants(nodeName).First();
			foreach (var card in cardPool.Elements())
			{
				string name = card.Attribute("c").Value;
				string set = card.Attribute("s").Value;
				int instance = int.Parse(card.Attribute("i").Value);
				int quantity = int.Parse(card.Attribute("n").Value);
				int quantityInUse = _CardCopiesRequiredInDecks(doc, name, set, instance);
				bool foil = card.Attribute("foil") != null && card.Attribute("foil").Value == "1" ? true : false;
				cards.Add(new CardStack(name, set, instance, quantity, quantityInUse, foil));
			}
		}

		private void _CardsToXml(List<CardStack> cards, XDocument doc)
		{
			_CardsToNodeInXml(cards, doc, "cardPool");
		}

		private void _NewCardsToXml(List<CardStack> cards, XDocument doc)
		{
			_CardsToNodeInXml(cards, doc, "newCardList");
		}

		private void _CardsToNodeInXml(List<CardStack> cards, XDocument doc, string nodeName)
		{
			var cardPool = doc.Descendants(nodeName).First();
			cardPool.ReplaceNodes(
				from el in cards
				select new
				XElement("card",
					new XAttribute("c", el.Name),
					new XAttribute("s", el.Set),
					new XAttribute("i", el.Instance.ToString()),
					new XAttribute("n", el.Quantity.ToString()),
					new XAttribute("foil", el.Foil ? "1" : "0")
				)
			);
		}
	}
}
