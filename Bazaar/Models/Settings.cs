﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Bazaar.Models
{
	static class Settings
	{
		private static string _settingsFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "settings.ini");

		public static string CardDirectoryPath
		{
			get
			{
				try
				{
					return File.ReadAllText(_settingsFilePath);
				}
				catch (FileNotFoundException)
				{
					return string.Empty;
				}
			}
			set
			{
				File.WriteAllText(_settingsFilePath, value);
			}
		}
	}
}
