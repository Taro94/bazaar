﻿using Avalonia.Data.Converters;
using CardManager;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bazaar.Models
{
	public class CardConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value is CardStack card)
			{
				string foiled = card.Foil ? ", Foil" : string.Empty;
				string result = $"{card.Name} ({card.Set}){foiled}, available: {card.Quantity-card.QuantityInUse}/{card.Quantity}";
				return result;
			}
			return string.Empty;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return false;
		}
	}
}
