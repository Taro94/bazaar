﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Data.Converters;
using Avalonia.Media.Imaging;
using Avalonia.Platform;
using Bazaar.Models;
using CardManager;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reflection;
using System.Text;
using Bazaar.Views;

namespace Bazaar.ViewModels
{
	class TradingViewModel : ViewModelBase
	{
		private string _path;
		private string _path2;
		private IBitmap _imageBitmap;
		private CardStack _selectedCardStack;
		private MainWindowViewModel _mainWindowViewModel;
		public string ImageFolderPath { get; set; }
		public IBitmap ImageBitmap
		{
			get => _imageBitmap; set
			{
				this.RaiseAndSetIfChanged(ref _imageBitmap, value);
			}
		}
		public CardStack SelectedCardStack
		{
			get => _selectedCardStack; set
			{
				this.RaiseAndSetIfChanged(ref _selectedCardStack, value);
				CardAvailable = (_selectedCardStack != null && _selectedCardStack.QuantityInUse < _selectedCardStack.Quantity && Quest.Cards.Contains(_selectedCardStack));
				CardAvailable2 = (_selectedCardStack != null && _selectedCardStack.QuantityInUse < _selectedCardStack.Quantity && Quest2.Cards.Contains(_selectedCardStack));
				ImageBitmap = TryToGetCardImage(ImageFolderPath, _selectedCardStack);
			}
		}

		//Save 1
		private List<CardStack> _cards;
		private string _nameFilter;
		private string _setFilter;
		private bool _cardAvailable;
		private bool _goldSendAvailable;
		private int _gold;
		private int _goldToSend;
		public Quest Quest { get; set; }
		public int GoldToSend
		{
			get => _goldToSend; private set
			{
				this.RaiseAndSetIfChanged(ref _goldToSend, value);
				GoldSendAvailable = value <= Gold;
			}
		}
		public bool GoldSendAvailable
		{
			get => _goldSendAvailable; private set
			{
				this.RaiseAndSetIfChanged(ref _goldSendAvailable, value);
			}
		}
		public bool CardAvailable
		{
			get => _cardAvailable; private set
			{
				this.RaiseAndSetIfChanged(ref _cardAvailable, value);
			}
		}
		public int Gold
		{
			get => _gold; private set
			{
				this.RaiseAndSetIfChanged(ref _gold, -1);
				this.RaiseAndSetIfChanged(ref _gold, value);
				GoldSendAvailable = value >= GoldToSend;
				Quest.Gold = value;
				UpdateQuest();
			}
		}
		public string NameFilter
		{
			get => _nameFilter; set
			{
				_nameFilter = value;
				FilterCards(_nameFilter, _setFilter);
			}
		}
		public string SetFilter
		{
			get => _setFilter; set
			{
				_setFilter = value;
				FilterCards(_nameFilter, _setFilter);
			}
		}
		public List<CardStack> Cards
		{
			get => _cards; private set
			{
				this.RaiseAndSetIfChanged(ref _cards, null);
				this.RaiseAndSetIfChanged(ref _cards, value);
			}
		}
		private void FilterCards(string name, string set)
		{
			CardStack temp = SelectedCardStack;
			name = name ?? string.Empty;
			set = set ?? string.Empty;
			Cards = Quest.Cards.Where(x => x.Name.ToLower().Contains(name.ToLower()) && x.Set.ToLower().Contains(set.ToLower())).ToList();
			if (Cards.Contains(temp))
				SelectedCardStack = temp;
		}
		private void UpdateQuest()
		{
			FilterCards(NameFilter, SetFilter);
		}
		public void SendCard()
		{
			CardStack taken = SelectedCardStack.TakeCardFromStack();
			if (SelectedCardStack.Quantity == 0)
				Quest.Cards.Remove(SelectedCardStack);

			CardStack newCardEntry = Quest.NewCards.Where(x => x.Name == taken.Name && x.Set == taken.Set && x.Instance == taken.Instance && x.Foil == taken.Foil).FirstOrDefault();
			if (newCardEntry != default(CardStack))
			{
				if (newCardEntry.Quantity <= taken.Quantity)
					Quest.NewCards.Remove(newCardEntry);
				else
					newCardEntry.TakeCardFromStack(taken.Quantity);
			}
			UpdateQuest();

			CardStack existingStack = Quest2.Cards.Where(x => x.Name == taken.Name && x.Set == taken.Set && x.Instance == taken.Instance && x.Foil == taken.Foil).FirstOrDefault();
			if (existingStack == default(CardStack))
				Quest2.Cards.Add(taken);
			else
				existingStack.AddCardToStack(taken.Quantity);
			newCardEntry = Quest2.NewCards.Where(x => x.Name == taken.Name && x.Set == taken.Set && x.Instance == taken.Instance && x.Foil == taken.Foil).FirstOrDefault();
			if (newCardEntry == default(CardStack))
				Quest2.NewCards.Add((CardStack)taken.Clone());
			else
				newCardEntry.AddCardToStack(taken.Quantity);
			UpdateQuest2();
		}
		public void SendGold()
		{
			Gold -= GoldToSend;
			Gold2 += GoldToSend;
			GoldSendAvailable = Gold >= GoldToSend;
		}

		//Save 2
		private List<CardStack> _cards2;
		private string _nameFilter2;
		private string _setFilter2;
		private bool _cardAvailable2;
		private bool _goldSendAvailable2;
		private int _gold2;
		private int _goldToSend2;
		public Quest Quest2 { get; set; }
		public int GoldToSend2
		{
			get => _goldToSend2; private set
			{
				this.RaiseAndSetIfChanged(ref _goldToSend2, value);
				GoldSendAvailable2 = value <= Gold2;
			}
		}
		public bool GoldSendAvailable2
		{
			get => _goldSendAvailable2; private set
			{
				this.RaiseAndSetIfChanged(ref _goldSendAvailable2, value);
			}
		}
		public bool CardAvailable2
		{
			get => _cardAvailable2; private set
			{
				this.RaiseAndSetIfChanged(ref _cardAvailable2, value);
			}
		}
		public int Gold2
		{
			get => _gold2; private set
			{
				this.RaiseAndSetIfChanged(ref _gold2, -1);
				this.RaiseAndSetIfChanged(ref _gold2, value);
				GoldSendAvailable = value >= GoldToSend2;
				Quest2.Gold = value;
				UpdateQuest2();
			}
		}
		public string NameFilter2
		{
			get => _nameFilter2; set
			{
				_nameFilter2 = value;
				FilterCards2(_nameFilter2, _setFilter2);
			}
		}
		public string SetFilter2
		{
			get => _setFilter2; set
			{
				_setFilter2 = value;
				FilterCards2(_nameFilter2, _setFilter2);
			}
		}
		public List<CardStack> Cards2
		{
			get => _cards2; private set
			{
				this.RaiseAndSetIfChanged(ref _cards2, null);
				this.RaiseAndSetIfChanged(ref _cards2, value);
			}
		}
		private void FilterCards2(string name, string set)
		{
			CardStack temp = SelectedCardStack;
			name = name ?? string.Empty;
			set = set ?? string.Empty;
			Cards2 = Quest2.Cards.Where(x => x.Name.ToLower().Contains(name.ToLower()) && x.Set.ToLower().Contains(set.ToLower())).ToList();
			if (Cards2.Contains(temp))
				SelectedCardStack = temp;
		}
		private void UpdateQuest2()
		{
			FilterCards2(NameFilter2, SetFilter2);
		}
		public void SendCard2()
		{
			CardStack taken = SelectedCardStack.TakeCardFromStack();
			if (SelectedCardStack.Quantity == 0)
				Quest2.Cards.Remove(SelectedCardStack);
			CardStack newCardEntry = Quest2.NewCards.Where(x => x.Name == taken.Name && x.Set == taken.Set && x.Instance == taken.Instance && x.Foil == taken.Foil).FirstOrDefault();
			if (newCardEntry != default(CardStack))
			{
				if (newCardEntry.Quantity <= taken.Quantity)
					Quest2.NewCards.Remove(newCardEntry);
				else
					newCardEntry.TakeCardFromStack(taken.Quantity);
			}
			UpdateQuest2();

			CardStack existingStack = Quest.Cards.Where(x => x.Name == taken.Name && x.Set == taken.Set && x.Instance == taken.Instance && x.Foil == taken.Foil).FirstOrDefault();
			if (existingStack == default(CardStack))
				Quest.Cards.Add(taken);
			else
				existingStack.AddCardToStack(taken.Quantity);
			newCardEntry = Quest.NewCards.Where(x => x.Name == taken.Name && x.Set == taken.Set && x.Instance == taken.Instance && x.Foil == taken.Foil).FirstOrDefault();
			if (newCardEntry == default(CardStack))
				Quest.NewCards.Add((CardStack)taken.Clone());
			else
				newCardEntry.AddCardToStack(taken.Quantity);
			UpdateQuest();
		}
		public void SendGold2()
		{
			Gold2 -= GoldToSend2;
			Gold += GoldToSend2;
			GoldSendAvailable2 = Gold2 >= GoldToSend2;
		}

		public TradingViewModel(Quest quest, Quest quest2, string path, string path2, MainWindowViewModel mainWindowViewModel)
		{
			_mainWindowViewModel = mainWindowViewModel;
			
			ImageFolderPath = Settings.CardDirectoryPath;
			ImageBitmap = GetDefaultCardImage();
			_path = path;
			_path2 = path2;

			CardAvailable = false;
			GoldSendAvailable = true;
			GoldToSend = 0;
			Quest = quest;
			NameFilter = string.Empty;
			SetFilter = string.Empty;
			Gold = Quest.Gold;

			CardAvailable2 = false;
			GoldSendAvailable2 = true;
			GoldToSend2 = 0;
			Quest2 = quest2;
			NameFilter2 = string.Empty;
			SetFilter2 = string.Empty;
			Gold2 = Quest2.Gold;
		}

		private Bitmap GetDefaultCardImage()
		{
			var assets = AvaloniaLocator.Current.GetService<IAssetLoader>();
			return new Bitmap(assets.Open(new Uri("avares://Bazaar/Assets/card_default.jpg")));
		}

		private Bitmap TryToGetCardImage(string path, CardStack card)
		{
			try
			{
				string setFolder;
				switch (card.Set)
				{
					case "5DN": setFolder = "FD"; break;
					case "CSP": setFolder = "CS"; break;
					case "4ED": setFolder = "4E"; break;
					case "5ED": setFolder = "5E"; break;
					case "6ED": setFolder = "6E"; break;
					case "7ED": setFolder = "7E"; break;
					case "8ED": setFolder = "8E"; break;
					case "9ED": setFolder = "9E"; break;
					case "DST": setFolder = "DS"; break;
					case "GPT": setFolder = "GP"; break;
					case "LEA": setFolder = "A"; break;
					case "ALL": setFolder = "AL"; break;
					case "ARN": setFolder = "AN"; break;
					case "APC": setFolder = "AP"; break;
					case "ATQ": setFolder = "AQ"; break;
					case "LEB": setFolder = "B"; break;
					case "CHR": setFolder = "CH"; break;
					case "DRK": setFolder = "DK"; break;
					case "EXO": setFolder = "EX"; break;
					case "FEM": setFolder = "FE"; break;
					case "HML": setFolder = "HL"; break;
					case "ICE": setFolder = "IA"; break;
					case "INV": setFolder = "IN"; break;
					case "JUD": setFolder = "JU"; break;
					case "LGN": setFolder = "LE"; break;
					case "LEG": setFolder = "LG"; break;
					case "MIR": setFolder = "MI"; break;
					case "MMQ": setFolder = "MM"; break;
					case "MRD": setFolder = "MR"; break;
					case "NMS": setFolder = "NE"; break;
					case "ODY": setFolder = "OD"; break;
					case "ONS": setFolder = "ON"; break;
					case "PO2": setFolder = "P2"; break;
					case "PTK": setFolder = "P3"; break;
					case "PLS": setFolder = "PS"; break;
					case "POR": setFolder = "PT"; break;
					case "PCY": setFolder = "PY"; break;
					case "3ED": setFolder = "R"; break;
					case "SCG": setFolder = "SC"; break;
					case "STH": setFolder = "SH"; break;
					case "S99": setFolder = "ST"; break;
					case "TMP": setFolder = "TE"; break;
					case "TOR": setFolder = "TO"; break;
					case "2ED": setFolder = "U"; break;
					case "UDS": setFolder = "UD"; break;
					case "ULG": setFolder = "UL"; break;
					case "USG": setFolder = "US"; break;
					case "VIS": setFolder = "VI"; break;
					default: setFolder = card.Set; break;
				}

				Bitmap bitmap;

				if (card.Instance == 1)
				{
					try
					{
						bitmap = new Bitmap(Path.Combine(ImageFolderPath, setFolder, card.ImageFileName));
						return bitmap;
					}
					catch (FileNotFoundException) { }
				}
				bitmap = new Bitmap(Path.Combine(ImageFolderPath, setFolder, card.ImageFileNameWithInstance));
				return bitmap;
			}
			catch (Exception ex)
			{
				if (ex is FileNotFoundException || ex is DirectoryNotFoundException || ex is NullReferenceException)
				{
					return GetDefaultCardImage();
				}
				throw;
			}
		}

		private void SaveFiles()
		{
			var gzipper = new GZip();
			using (var saveFile = new FileInfo(_path).OpenWrite())
			{
				using (Stream compressedStream = gzipper.CreateGZip(Quest.ToMemoryStream()))
				{
					compressedStream.CopyTo(saveFile);
				}
			}
			using (var saveFile = new FileInfo(_path2).OpenWrite())
			{
				using (Stream compressedStream = gzipper.CreateGZip(Quest2.ToMemoryStream()))
				{
					compressedStream.CopyTo(saveFile);
				}
			}
			_mainWindowViewModel.BackToFileSelection();
			//((MainWindowViewModel)App.Current.MainWindow.DataContext).BackToFileSelection();
		}
	}
}
