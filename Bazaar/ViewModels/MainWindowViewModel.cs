﻿using Avalonia.Controls;
using CardManager;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;

namespace Bazaar.ViewModels
{
	public class MainWindowViewModel : ViewModelBase
	{
		ViewModelBase _content;
		public ViewModelBase Content
		{
			get => _content;
			private set => this.RaiseAndSetIfChanged(ref _content, value);
		}

		public MainWindowViewModel()
		{
			Content = new FileSelectionViewModel(this);
		}

		public void LoadTradingView(Quest quest, Quest quest2, string path, string path2)
		{
			Content = new TradingViewModel(quest, quest2, path, path2, this);
		}

		public void LoadSettingsView()
		{
			Content = new AppSettingsViewModel(this);
		}

		public void Exit()
		{
			Environment.Exit(0);
		}

		public void BackToFileSelection()
		{
			Content = new FileSelectionViewModel(this);
		}
	}
}
