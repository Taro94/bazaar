﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Media.Imaging;
using Avalonia.Platform;
using Bazaar.Models;
using CardManager;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reflection;
using System.Text;
using Avalonia.Controls.ApplicationLifetimes;
using Bazaar.Views;

namespace Bazaar.ViewModels
{
	class AppSettingsViewModel : ViewModelBase
	{
		private string _path;
		private MainWindowViewModel _mainWindowViewModel;
		public string Path
		{
			get => _path; set { this.RaiseAndSetIfChanged(ref _path, value); }
		}

		public AppSettingsViewModel(MainWindowViewModel mainWindowViewModel)
		{
			_mainWindowViewModel = mainWindowViewModel;
			Path = Settings.CardDirectoryPath;
		}

		public void SaveChanges()
		{
			Settings.CardDirectoryPath = Path;
			_mainWindowViewModel.BackToFileSelection();
		}

		private async void BrowseForDirectory()
		{
			if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
			{
				var openDialog = new OpenFolderDialog();
				openDialog.Title = "Select Forge card picture directory";
				var selected = await openDialog.ShowAsync(desktop.MainWindow);
				if (selected != null && selected.Length != 0)
				{
					Path = selected;
				}
			}
		}
	}
}
