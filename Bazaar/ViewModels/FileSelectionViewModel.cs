﻿using Avalonia.Controls;
using CardManager;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Xml;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;

namespace Bazaar.ViewModels
{
	class FileSelectionViewModel : ViewModelBase
	{
		private string _path;
		private string _path2;
		private string _feedback;
		private MainWindowViewModel _mainWindowViewModel;
		public string Path { get => _path; private set => this.RaiseAndSetIfChanged(ref _path, value); }
		public string Path2 { get => _path2; private set => this.RaiseAndSetIfChanged(ref _path2, value); }
		public string Feedback { get => _feedback; private set => this.RaiseAndSetIfChanged(ref _feedback, value); }

		public ReactiveCommand<Unit,Unit> LoadFile { get; }

		public FileSelectionViewModel(MainWindowViewModel mainWindowViewModel)
		{
			_mainWindowViewModel = mainWindowViewModel;
			Path = string.Empty;
			Path2 = string.Empty;

			var loadEnabled = this.WhenAnyValue(
				x => x.Path,
				y => y.Path2,
				(x, y) => !string.IsNullOrWhiteSpace(x) && !string.IsNullOrWhiteSpace(y));

			LoadFile = ReactiveCommand.Create(() => ProcessFiles(), loadEnabled);
		}

		private void ProcessFiles()
		{
			Feedback = string.Empty;
			string fullPath = System.IO.Path.GetFullPath(Path);
			string fullPath2 = System.IO.Path.GetFullPath(Path2);

			if (fullPath == fullPath2)
			{
				Feedback = "Two different files must be selected.";
				return;
			}

			if (!File.Exists(fullPath) || !File.Exists(fullPath2))
			{
				Feedback = "At least one file path is invalid.";
				return;
			}

			//Stworzenie obiektu do pakowania i rozpakowywania sejwa
			var gzipper = new GZip();

			//Uzyskanie streamu z wypakowanym sejwem
			Stream decompressedStream, decompressedStream2;
			try
			{
				using (var saveFile = new FileInfo(fullPath).OpenRead())
				{
					decompressedStream = gzipper.ExtractGZip(saveFile);
				}
				using (var saveFile = new FileInfo(fullPath2).OpenRead())
				{
					decompressedStream2 = gzipper.ExtractGZip(saveFile);
				}

				//Stworzenie obiektu do zarządzania sejwem na podstawie wypakowanego strumienia
				var quest = new Quest(decompressedStream);
				var quest2 = new Quest(decompressedStream2);
				_mainWindowViewModel.LoadTradingView(quest, quest2, fullPath, fullPath2);
			}
			catch (InvalidDataException)
			{
				Feedback = "At least one file is not a valid Forge quest file.";
			}
			catch (XmlException)
			{
				Feedback = "At least one file is corrupted.";
			}
		}

		private void GoToSettings()
		{
			_mainWindowViewModel.LoadSettingsView();
		}

		private async void BrowseForFile()
		{
			if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
			{
				var openDialog = new OpenFileDialog();
				openDialog.Title = "Select Forge quest file";
				openDialog.AllowMultiple = false;
				openDialog.Filters.Add(new FileDialogFilter
				{
					Name = "Forge quest save files",
					Extensions = new List<string> {"dat"}
				});
				
				var files = await openDialog.ShowAsync(desktop.MainWindow);
				if (files != null && files.Length != 0)
				{
					Path = files[0];
				}
			}
		}

		private async void BrowseForFile2()
		{
			if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
			{
				var openDialog = new OpenFileDialog();
				openDialog.Title = "Select Forge quest file";
				openDialog.AllowMultiple = false;
				openDialog.Filters.Add(new FileDialogFilter
				{
					Name = "Forge quest save files",
					Extensions = new List<string> {"dat"}
				});

				var files = await openDialog.ShowAsync(desktop.MainWindow);
				if (files != null && files.Length != 0)
				{
					Path2 = files[0];
				}
			}
		}
	}
}
