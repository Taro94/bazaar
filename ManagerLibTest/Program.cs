﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using CardManager;

namespace ManagerLibTest
{
	class Program
	{
		static void Main(string[] args)
		{
			//Ścieżka do sejwa
			string filePath = @"E:\Taro BG.dat";

			//Stworzenie obiektu do pakowania i rozpakowywania sejwa
			var gzipper = new GZip();

			//Uzyskanie streamu z wypakowanym sejwem
			Stream decompressedStream;
			using (var saveFile = new FileInfo(filePath).OpenRead())
			{
				decompressedStream = gzipper.ExtractGZip(saveFile);
			}

			//Stworzenie obiektu do zarządzania sejwem na podstawie wypakowanego strumienia
			var quest = new Quest(decompressedStream);

			//Przykład: dodanie 5 egzemplarzy karty "Woolly Loxodon"
			CardStack loxodon = quest.Cards.Where(x => x.Name == "Woolly Loxodon").FirstOrDefault();
			if (loxodon != null)
			{
				loxodon.AddCardToStack(5);
				CardStack stackTaken = loxodon.TakeCardFromStack(5);
			}

			//Przykład: ustawienie golda w sejwie na 5
			quest.Gold = 5;

			//Przykład: wyświetlenie wszystkich kart wraz z ich nazwą, setem, numerem instancji danej karty,liczebnością łącznie oraz wykorzystywaną w taliach, a także czy karta jest foliowana
			foreach (var c in quest.Cards)
			{
				Console.WriteLine($"{c.Name}, {c.Set}, {c.Instance}, {c.QuantityInUse}/{c.Quantity}, {c.Foil}");
			}
			Console.WriteLine(quest.Gold);

			//Zapisanie sejwa jako strumień (quest.ToMemoryStream() ), skompresowanie go oraz nadpisanie pliku sejwa
			using (var saveFile = new FileInfo(filePath).OpenWrite())
			{
				using (Stream compressedStream = gzipper.CreateGZip(quest.ToMemoryStream()))
				{
					compressedStream.CopyTo(saveFile);
				}
			}

			Console.ReadLine();
		}
	}
}
